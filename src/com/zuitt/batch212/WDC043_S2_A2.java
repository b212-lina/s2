package com.zuitt.batch212;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class WDC043_S2_A2 {

    public static void main(String[] args) {

        //  Prime Numbers

        int[] primeNumbers = new int[5];

        primeNumbers[0] = 2;
        primeNumbers[1] = 3;
        primeNumbers[2] = 5;
        primeNumbers[3] = 7;
        primeNumbers[4] = 11;

        System.out.println("The first prime number is: " + primeNumbers[0]);
        System.out.println("The second prime number is: " + primeNumbers[1]);
        System.out.println("The third prime number is: " + primeNumbers[2]);
        System.out.println("The fourth prime number is: " + primeNumbers[3]);
        System.out.println("The fifth prime number is: " + primeNumbers[4]);

        //  ArrayList

        ArrayList<String> names = new ArrayList<>(Arrays.asList("John", "Jane", "Chloe", "Zoey"));

        System.out.println("My friends are: " + names);

        //  HashMap

        HashMap<String, Integer> inventory = new HashMap<>();

        inventory.put("Toothpaste", 15);
        inventory.put("Toothbrush", 20);
        inventory.put("Soap", 12);

        System.out.println("Our current inventory consists of: " + inventory);

    }

}
