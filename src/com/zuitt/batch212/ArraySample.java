package com.zuitt.batch212;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class ArraySample {

    public static void main(String[] args) {

        //  Arrays = fixed/limited collections of data.
        //  2^31 = 2,147,483,648 elements

        //  Declaration
        int[] intArray = new int[3];

        intArray[0] = 10;
        intArray[1] = 20;
        intArray[2] = 30;

        System.out.println(intArray[2]);

        //  other syntax used to declare arrays
        int intSample[] = new int[2];
        intSample[0] = 50;

        System.out.println(intSample[0]);

        //  String array
        String stringArray[] = new String[3];

        stringArray[0] = "John";
        stringArray[1] = "Jane";
        stringArray[2] = "Joe";

        //  Declaration with initialization
        int[] intArray2 = {100, 200, 300, 400, 500};

        System.out.println(Arrays.toString(intArray2));

        //  Method used in arrays
        //  sort()
        Arrays.sort(stringArray);
        System.out.println(Arrays.toString(stringArray));

        //  binary search
        String searchTerm = "John";
        int binaryResult = Arrays.binarySearch(stringArray, searchTerm);

        System.out.println(binaryResult);

        //  ArrayLists - resizable arrays, wherein elements can be added or removed whenever it is needed.

        //  Declaration
        ArrayList<String> students = new ArrayList<>();

        //  Adding elements
        students.add("John");
        students.add("Paul");

        System.out.println(students);

        //  Access elements to an ArrayList
        System.out.println(students.get(0));

        //  Update/changing of element
        students.set(1, "George");
        System.out.println(students);

        //  Removing an element
        students.remove(1);
        System.out.println(students);

        //  Removing all elements in an ArrayList using clear()
        students.clear();
        System.out.println(students);

        //  Getting the number of elements in an ArrayList using size()
        //  Get the length of an ArrayList
        System.out.println(students.size());

        //  We can also declare and initialize values
        ArrayList<String> employees = new ArrayList<>(Arrays.asList("June", "Albert"));

        //  Hashmaps -> key: value pair
        HashMap<String, String> employeeRole = new HashMap<>();

        // Adding fields
        employeeRole.put("Captain", "Luffy");
        employeeRole.put("Doctor", "Chopper");
        employeeRole.put("Navigator", "Nami");

        System.out.println(employeeRole);

        //  Retrieving field value
        System.out.println(employeeRole.get("Captain"));

        //  Removing elements
        employeeRole.remove("Doctor");
        System.out.println(employeeRole);

        //  Retrieving hashmap keys/fields
        System.out.println(employeeRole.keySet());

        //  With integers as values
        HashMap<String, Integer> grades = new HashMap<>();

        grades.put("English", 89);
        grades.put("Math", 93);
        System.out.println(grades);

        // With ArrayLists
        HashMap<String, ArrayList<Integer>> subjectGrades = new HashMap<>();

        ArrayList<Integer> gradesListA = new ArrayList<>(Arrays.asList(75,80,90));

        ArrayList<Integer> gradesListB = new ArrayList<>(Arrays.asList(89, 87, 85));

        subjectGrades.put("Joe", gradesListA);
        subjectGrades.put("Jane", gradesListB);

        System.out.println(subjectGrades);


    }

}
